function k = rbf_hist_kern(u,v)
uu = hist_kernel(u,u);
uv = hist_kernel(u,v);
vv = hist_kernel(v,v);
m = size(u,1);
n = size(v,1);
k = ones(m,n);
for i = 1:m
	for j = 1:n
		k(i,j)=exp(2*uv(i,j)-uu(i,i)-vv(j,j));
	end
end

end


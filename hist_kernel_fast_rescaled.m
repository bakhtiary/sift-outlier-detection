function k = hist_kernel_fast_rescaled(u,v)
	k = hist_kernel_fast(u,v);
	k = k/max(k(:));


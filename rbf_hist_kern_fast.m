function k = rbf_hist_kern_fast(u,v)
uu = sum(u,2);
uv = hist_kernel_fast(u,v)*2;
vv = sum(v,2);
m = size(u,1);
n = size(v,1);
k = ones(m,n);

r1 = bsxfun(@minus,uv,vv');
r2 = bsxfun(@minus,r1,uu);
k = exp(r2);

end


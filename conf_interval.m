function interval = conf_interval(values, prob)
	x = values;
	SEM = std(x)/sqrt(length(x));               % Standard Error
	ts = tinv([1-prob  prob],length(x)-1)      % T-Score
	CI = mean(x) + ts*SEM;                      % Confidence Intervals

	interval = CI;
end

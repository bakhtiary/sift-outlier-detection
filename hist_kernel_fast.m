function k = hist_kernel_fast(u,v)
m = size(u,1);
n = size(v,1);
o = size(u,2);
k = ones(m,n);

u2 = reshape(u,m,1,o);
v2 = reshape(v,1,n,o);

c = bsxfun(@min,u2,v2);
k = sum(c,3);
end

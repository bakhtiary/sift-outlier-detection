function k = hist_kernel_fast(u,v)
m = size(u,1);
n = size(v,1);
k = ones(m,n);
for i = 1:m
	for j = 1:n
		k(i,j) = sum (min(u(i,:),v(j,:)));
	end

end

end

function [trainData,trainLabel,testData,testLabel] = get_dataset(num)	
	stream = RandStream('mt19937ar','Seed',num+12345);
	formatSpec = '%f';
	DBDirname = 'bow_1000_dense/';
	dirs = dir(DBDirname);
	allClasses = randperm(stream,257,20);
	testClasses = allClasses(1:10);
	trainClasses = allClasses(11:20);
	
	testData = [];
	trainData = [];
	testLabel = [];
	for i = 1:numel(trainClasses) 
		full_dir_name = strcat(DBDirname,dirs(trainClasses(i)+2).name);
		files = dir (full_dir_name);
		num_files = size(files);
		allFiles = randperm(stream,num_files(1)-2);
		testFiles = allFiles(1:50);
		trainFiles = allFiles(51:end);
	
		for j = 1:numel(testFiles)
			the_file = files(testFiles(j)+2);
			full_file_name = strcat(full_dir_name,'/',the_file.name);
			fileID = fopen(full_file_name);
			data = fscanf(fileID,formatSpec);
			testData = [testData; data'];
			testLabel = [testLabel; 1];
		end
	
		for j = 1:numel(trainFiles)
			the_file = files(trainFiles(j)+2);
			full_file_name = strcat(full_dir_name,'/',the_file.name);
			fileID = fopen(full_file_name);
			data = fscanf(fileID,formatSpec);
			trainData = [trainData; data'];
		end
	
	
	end
	for i = 1:numel(testClasses) 
		full_dir_name = strcat(DBDirname,dirs(testClasses(i)+2).name);
		files = dir (full_dir_name);
		num_files = size(files);
		allFiles = randperm(stream,num_files(1)-2);
		testFiles = allFiles(1:50);
	
		for j = 1:numel(testFiles)
			the_file = files(testFiles(j)+2);
			full_file_name = strcat(full_dir_name,'/',the_file.name);
			fileID = fopen(full_file_name);
			data = fscanf(fileID,formatSpec);
			testData = [testData; data'];
			testLabel = [testLabel; 0];
		end
	
	end
	
	
	trainLabel = ones(size(trainData,1),1);

end



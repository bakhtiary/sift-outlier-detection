
results = [];
if ~exist('cache', 'dir')
	mkdir ('cache')
end

for run_num = 1:50
	filename = strcat('cache/',num2str(run_num));
	if exist(strcat(filename,'.mat'))
		load (filename,'trainData','trainLabel','testData','testLabel');
	else
		[trainData,trainLabel,testData,testLabel] = get_dataset(run_num);
		save(filename,'trainData','trainLabel','testData','testLabel');
	end
	rng(123)
	model = fitcsvm(trainData,trainLabel,'KernelFunction','hist_kernel_fast_rescaled','nu',0.5,'Standardize',false);
	[~,scores] = predict(model,testData);
	[~,~,~,AUC] = perfcurve(testLabel,scores,1)
	results = [results AUC];
end
mean(results)
std(results)
save('res/hist_kernel_fast_rescaled_svm_nu_0.5_Standarize_false_results.mat','results')


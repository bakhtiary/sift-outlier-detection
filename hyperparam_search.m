function results = hyperparam_search(trainData,trainLabel,testData,testLabel)
nus = [0.01,0.1,0.5,0.9,0.99,0.999];
results = [];
for nu = nus;
model = fitcsvm(trainData,trainLabel,...
	'KernelFunction','hist_kernel_fast_rescaled',...
	'Standardize',true,...
	'nu',nu );

[~,scores] = predict(model,testData);
[~,~,~,AUC] = perfcurve(testLabel,scores,1)

results = [results AUC];
end
end
